var controller = {};

controller.setView = function (viewId) {
    $("main > div").hide();
    $("main > #" + viewId).show();
};