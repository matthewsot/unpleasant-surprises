var quiz = {};

quiz.questions = [
    {
        key: "afca2bf4-b829-47c7-bec0-028760b8d39a", //This could be more descriptive, like "tax-burden"
        question: "What is the total tax burden on the average American?",
        choices: [
            {
                text: "About 15%",
                profile: { "anti-tax": 4 }
            },
            {
                text: "About 30%",
                profile: { "anti-tax": 3 }
            },
            {
                text: "About 45%",
                profile: { "anti-tax": 2 }
            },
            {
                text: "About 60%",
                profile: { "anti-tax": 1 }
            }
        ],
        correctChoice: 1,
        explanation: "The Tax Foundation estimated 31.5% based on 2014 data.",
        source: "http://taxfoundation.org/article/comparison-tax-burden-labor-oecd-0"
    }
];

quiz.questions.getQuestion = function (key) {
    if (!isNaN(key)) {
        return quiz.questions[key];
    } else {
        for (var i = 0; i < quiz.questions.length; i++) {
            if (quiz.questions[i].key === key) {
                return quiz.questions[i];
            }
        }
    }

    return null;
};

quiz.getAnswers = function () {
    return JSON.parse(sessionStorage.getItem("quiz-answers")) || {};
};

quiz.answerQuestion = function (key, choice, answerType) {
    var question = quiz.questions.getQuestion(key);

    if (question === null) {
        alert("Question not found");
        return -1;
    }

    var existingAnswers = quiz.getAnswers();

    existingAnswers[key] = existingAnswers[key] || {};
    existingAnswers[key][answerType] = choice;

    sessionStorage.setItem("quiz-answers", JSON.stringify(existingAnswers));

    return choice === question.correctChoice;
};

quiz.setQuestion = function (key, answerType) {
    var question = quiz.questions.getQuestion(key);

    if (question === null) {
        alert("Question not found");
        return -1;
    }

    $("#question-title").text(question.question);
    $("#question-subtitle").html(answerType === "prediction" ? "What do you think is the <i>correct</i> answer?" : "What do you think the answer <i>should</i> be?");
    $("#question-image").attr("src", question.image);
    $("#question-options").html(""); //Options

    for (var i = 0; i < question.choices.length; i++) {
        var choiceLetter = String.fromCharCode(65 + i);
        $("#question-options").append($("<li></li>").text(choiceLetter + ") " + question.choices[i].text));
    }

    return question;
};

quiz.calculateProfile = function () {
    var answers = quiz.getAnswers();
    var profile = {};

    var keys = Object.keys(answers);
    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        if (answers[key]["prediction"] === undefined || answers[key]["preference"] === undefined) continue;

        var question = quiz.questions.getQuestion(key);

        var prediction = question.choices[answers[key]["prediction"]];
        var preference = question.choices[answers[key]["preference"]];

        var profileKeys = Object.keys(prediction.profile);
        for (var d = 0; d < profileKeys.length; d++) {
            var profileKey = profileKeys[d];
            profile[profileKey] = profile[profileKey] || 0;
            profile[profileKey] += preference.profile[profileKey] - prediction.profile[profileKey]; //Positive = "world should be more libertarian"
        }
    }
    return profile;
};